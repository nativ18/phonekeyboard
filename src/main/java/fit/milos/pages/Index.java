package fit.milos.pages;

import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.InjectComponent;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.corelib.components.EventLink;
import org.apache.tapestry5.corelib.components.Zone;

public class Index {
	
	@Persist
	@Property
	private String text;
	@Persist
	private long pressedBefore;
	@Persist
	private int lastKeyPressed;
	@InjectComponent
	private Zone textZone;
	
	private String[] values = {" ", ".,!?", "ABC", "DEF", "GHI", "JKL", "MNO", "PQRS", "TUV", "WXYZ"};
	
	//Metoda za resetovanje vrijednosti unijetog teksta
	@OnEvent(component="delete")
	Object onDeleteText(){
		text = "";
		return textZone.getBody();
	}
	
	@Component(parameters={"event=buttonPressed", "zone=textZone"})
	private EventLink button1, button2, button3, button4, button5, 
					button6, button7, button8, button9, button0;
	
	//Metoda za upis novog slova
	private Object onButtonPressed(String key){
		//Cuvamo vrijeme kada je pritisnuto dugme
		long pressedAt = System.currentTimeMillis();
		if(text == null) text = "";
		int i = Integer.parseInt(key);
		//Ukoliko se pritisnuto dugme razlikuje od prethodnog ispisuje se novo slovo
		if(i != lastKeyPressed)
			text += getChar(values[i], 0);
		else {
			//Ukoliko je pritisnuto isto dugme, provjeravamo da li je proslo vise od jedne sekunde
			//Ako jeste, dodaje se novo slovo na tekst
			if (pressedAt - pressedBefore > 1000)
				text += getChar(values[i], 0);
			else {
				//Ukoliko je isto dugme pritisnuto u manjem vremenskom razmaku od jedne sekunde
				//poslednje slovo se mijenja za sledece u nizu iz odgovarajuceg dugmeta
				char lastChar = text.charAt(text.length()-1);
				String restOfText = text.substring(0, text.length()-1);
				text = restOfText + giveNextChat(values[i], lastChar);
			}
		}
		//Cuvamo poslednje pritisnuto dugme i vrijeme
		lastKeyPressed = i;
		pressedBefore = pressedAt;
		return textZone.getBody();
	}
	
	//Pronalazi odgovarajuce slovo
	private String getChar(String t, int i){
		if(i!=0) i = i % t.length();
		return t.charAt(i) + "";
	}
	
	//Vraca sledece slovo
	private char giveNextChat(String val, char lastChar){
		int i = val.indexOf(lastChar) + 1;
		if(i >= val.length()) i = 0;
		return val.charAt(i);
	}
	
}
